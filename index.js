
const FIRST_NAME = "Lorena";
const LAST_NAME = "Stan";
const GRUPA = "1083";

/**
 * Make the implementation here
 */
function initCaching() {
   
    var page = {}; 
    page.about = 0;
    page.home = 0; 
    page.contact =0;
    page.counter = 0; 

    function pageAccessCounter(str = '')
    {
        if (str == 'about' || str == 'ABOUT') {
            page.about += 1;
            page.counter += 1;
        }
 
        else if (str == 'contact') {
            page.contact += 1;
            page.counter += 1;
        } 

        else if (str == '') {
            page.home += 1;
            page.counter += 1;
       }

         return page;
    }
 
    function getCache()
    {
        return page;
    }
 
    return {
        pageAccessCounter,
        getCache
    };
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

